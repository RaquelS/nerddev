<?php
namespace NERDDEV\Controladores;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use NERDDEV\Entidades\Usuario;
use NERDDEV\Modelos\Usuarios;
use NERDDEV\Entidades\Sessao;
use NERDDEV\Entidades\Vaga;
use NERDDEV\Modelos\Vagas;

class ControladorUsuario
{

    private $response;
    private $twig;
    private $request;
    private $session;


    function __construct(Response $response, Environment $twig, Request $request)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->session = new Sessao();
    }

    public function telaCadastrarUsuario()
    {
        return $this->response->setContent($this->twig->render('cadastrarUsuario.html', ["session" => $this->session]));
    }

    public function cadastrarUsuario()
    {
        $msg = "";
        $dados = $this->request->get('dados');

        if (isset($dados) && !empty($dados)) {
            $nome = $dados[0]['value'];
            $email = $dados[1]['value'];
            $senha = md5($dados[2]['value']);
            $genero = $dados[3]['value'];
            $nacionalidade = $dados[4]['value'];
            $dataNascimento = $dados[5]['value'];
            $endereco = $dados[6]['value'];
            $cidadeEstado = $dados[7]['value'];
            $telefone = $dados[8]['value'];

            $usu = new Usuario(
                $nome,
                $email,
                $senha,
                $genero,
                $nacionalidade,
                $dataNascimento,
                $endereco,
                $cidadeEstado,
                $telefone
            );
            $usuModel = new Usuarios();
            $teste = $usuModel->cadastrarUsuario($usu);
            if ($teste != null) {
                $msg = "<script>window.location.href = '/login'</script>";
            } else {
                $msg = "ERRO AO CADASTRAR O USARIO";
            }
        } else {
            $msg = "CAMPOS OBRIGATORIOS NÃO PREENCHIDOS";
        }

        return $this->response->setContent($msg);
    }

    public function login()
    {
        return $this->response->setContent($this->twig->render('login.html', ["session" => $this->session]));
    }

    public function efetuarLogin()
    {
        $msg = "";
        $dados = $this->request->get('dados');

        if (isset($dados) && !empty($dados)) {
            $email = $dados[0]['value'];
            $senha = md5($dados[1]['value']);

            $usuModel = new Usuarios();
            $dadosUsu = $usuModel->buscaLogin($email);
            if (!empty($dadosUsu['nome'])) {
                if ($senha == $dadosUsu['senha']) {
                    $this->session->add("usuario", $dadosUsu['nome']);
                    $this->session->add("email", $dadosUsu['email']);
                    $this->session->add("nivelAcesso", $dadosUsu['nivelAcesso']);
                    $this->session->add("id", $dadosUsu['id']);

                    if (isset($dadosUsu['curriculo'])) {
                        $this->session->add("curriculo", $dadosUsu['curriculo']);
                    }

                    if ($this->session->get("nivelAcesso") == 0) {
                        return $this->response->setContent("<script>window.location.href = '/' </script>");
                    } else {
                        return $this->response->setContent("<script>window.location.href = '/painel_controle' </script>");
                    }
                } else {
                    $msg = "USUÁRIO OU SENHA INCORRETOS";
                }
            } else {
                $msg = "USUÁRIO OU SENHA INCORRETOS";
            }
        } else {
            $msg = "CAMPOS OBRIGATORIOS NAO PREENCHIDOS";
        }

        return $this->response->setContent($msg);
    }

    public function logout()
    {
        if ($this->session->existe("usuario")) {
            $this->session->del();
            return $this->response->setContent("<script>window.location.href = '/' </script>");
        }
    }

    public function telaPerfil()
    {
        if ($this->session->existe("usuario")) {
            $usuModel = new Usuarios();
            $dadosUsu = $usuModel->buscaUsuario($this->session->get('id'));

            return $this->response->setContent($this->twig->render(
                'perfilUsuario.html',
                ["session" => $this->session, "usuario" => $dadosUsu]
            ));
        } else {
            return $this->response->setContent($this->twig->render("home.html"));
        }
    }

    public function cadastrarCurriculo()
    {
        $msg = "";
        $dados = $this->request->files->get('dados');
        $dados = $_GET['id'];
        print_r(var_dump($dados));
        return;
    }

    public function painelAdmin()
    {
        $modelVaga = new Vagas();
        $vagasInativas = $modelVaga->buscarVagasStatus("0");
        $vagasAbertas = $modelVaga->buscarVagasStatus("1");
        $vagasFechadas = $modelVaga->buscarVagasStatus("2");

        return $this->response->setContent($this->twig->render('painelAdmin.html', ["session" => $this->session, 
        'vagasinativas'=> $vagasInativas,
        'vagasabertas'=>$vagasAbertas,
        'vagasfechadas'=>$vagasFechadas]));
    }
}
