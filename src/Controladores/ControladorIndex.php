<?php
namespace NERDDEV\Controladores;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use NERDDEV\Entidades\Sessao;

class ControladorIndex{

    private $response;
    private $twig;
    private $session;

    function __construct(Response $response, Environment $twig)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->session = new Sessao();
    }

    public function home(){
        return $this->response->setContent($this->twig->render('home.html', ["session" => $this->session]));
    }
    
}
