<?php
namespace NERDDEV\Controladores;

use NERDDEV\Entidades\Sessao;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use NERDDEV\Modelos\Vagas;
use NERDDEV\Entidades\Vaga;

class ControladorVaga
{

    private $response;
    private $twig;
    private $request;
    private $session;

    function __construct(Response $response, Environment $twig, Request $request)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->session = new Sessao();
    }

    public function telaCadastrarVaga()
    {
        return $this->response->setContent($this->twig->render('cadastrarVaga.html', ["session" => $this->session]));
    }
    public function cadastrarVaga()
    { 
        $msg = '';
        $modelVaga = new Vagas();
        $dados= $this->request->get('dados');

        if (isset($dados) && !empty($dados)) {
            $vaga = new Vaga($dados[0]['value'],
                    $dados[1]['value'],
                    $dados[2]['value'],
                    $dados[3]['value'],
                    null,
                    $dados[4]['value'],
                    0
                    );

           $teste = $modelVaga->cadastrarVaga($vaga); 

           if($teste != null){
                $msg = "VAGA CADASTRADA COM SUCESSO";
           }else{
                $msg = "ERRO AO CADASTRAR A VAGA";
           }
        }else{
            $msg = "CAMPOS OBRIGATORIOS NÃO PREENCHIDOS";
        }

        return $this->response->setContent($msg);

    }
    public function telaVaga(){

        $id = $this->request->get('id');
        $id = $_GET['id'];
        
        $modelVaga = new  Vagas();
        $vaga = $modelVaga->buscarVaga($id);

            return $this->response->setContent($this->twig->render('vaga.html', ["session" => $this->session, 'vaga'=>$vaga]));
        
    }
}
