<?php

namespace NERDDEV\Modelos;


use NERDDEV\Util\Conexao;
use NERDDEV\Entidades\Usuario;
use PDO;

class Usuarios
{

    public function CadastrarUsuario(Usuario $usu)
    {

        try {

            $sql = 'insert into usuario(nome, email, senha, dataNascimento, endereco,
                cidadeEstado, telefone, genero, nivelAcesso)
                values(:nome, :email, :senha, :dataNascimento, :endereco,
                :cidadeEstado, :telefone, :genero, :nivelAcesso ); ';

            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $usu->getNome());
            $p_sql->bindValue(':email', $usu->getEmail());
            $p_sql->bindValue(':senha', $usu->getSenha());
            $p_sql->bindValue(':dataNascimento', $usu->getdataNascimento());
            $p_sql->bindValue(':endereco', $usu->getendereco());
            $p_sql->bindValue(':cidadeEstado', $usu->getCidadeEstado());
            $p_sql->bindValue(':telefone', $usu->getTelefone());
            $p_sql->bindValue(':genero', $usu->getGenero());
            $p_sql->bindValue(':nivelAcesso', $usu->getNivelAcesso());

            if ($p_sql->execute()) {
                return true;
            }

            return null;
        } catch (Exception $exc) {
            return null;
        }
    }

    public function verificaEmail($email)
    {
        try {
            $sql = 'select count(email) from usuario where email like :email;';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':email', $email);

            if ($p_sql->execute()) {
                return $p_sql->fetch();
            }
            return null;
        } catch (Exception $exc) {
            return null;
        }
    }


    public function buscaLogin($email)
    {
        try {
            $sql = 'select email,senha,id,nivelAcesso,nome,curriculo from usuario where email like :email;';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':email', $email);

            if ($p_sql->execute()) {
                return $p_sql->fetch(PDO::FETCH_ASSOC);
            }
            return null;
        } catch (Exception $exc) {
            return null;
        }
    }

    public function buscaUsuario($id)
    {
        try {

            $sql = 'select nome, email, dataNascimento, 
            endereco, cidadeEstado, telefone, genero,curriculo from usuario where id = :id;';

            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            if ($p_sql->execute()) {
               
                return $p_sql->fetch(PDO::FETCH_ASSOC);
            }
        } catch (Exception $exc) {
            return null;
        }
    }

    
}
