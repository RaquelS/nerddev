<?php

namespace NERDDEV\Modelos;


use NERDDEV\Util\Conexao;
use PDO;

class Vagas
{
    public function cadastrarVaga($vaga)
    {
        try{
            $sql = 'insert into vaga(cargo, descricao, qtdVagas, localTrabalho, dataFechamento, salario, statusVaga)
                    values(:cargo, :descricao, :qtdVagas, :localTrabalho, :dataFechamento, :salario, :status);';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':cargo', $vaga->getCargo());
            $p_sql->bindValue(':descricao', $vaga->getDescricao());
            $p_sql->bindValue(':qtdVagas', $vaga->getQtdVagas());
            $p_sql->bindValue(':localTrabalho', $vaga->getLocalTrabalho());
            $p_sql->bindValue(':dataFechamento', $vaga->getDataFechamento());
            $p_sql->bindValue(':salario', $vaga->getSalario());
            $p_sql->bindValue(':status', $vaga->getStatus());

            if($p_sql->execute()){
                return true;
            }

            return null;
        }catch(Exception $e){
            return null;
        }
     }
    public function excluirVaga()
    { }
    public function editarVaga()
    { }
    public function buscarVaga($id)
    { 
        try{
            $sql= 'select * from vaga where id = :id;';
            $p_sql= Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);

            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            if ($p_sql->execute()) {
                return $p_sql->fetch();
            }

            return null;
        }catch(Exception $e){
            return null;
        }
    }
    public function buscarVagas()
    { 
        try{
            $sql= 'select * from vaga;';
            $p_sql= Conexao::getInstancia()->prepare($sql);
           
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            if ($p_sql->execute()) {
                return $p_sql->fetchAll();
            }

            return null;
        }catch(Exception $e){
            return null;
        }
    }
    public function buscarVagasStatus( $status)
    {
        try {
            $sql = 'select * from vaga where statusVaga = :status';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':status', $status);
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            if ($p_sql->execute()) {
                return $p_sql->fetchAll();
            }
            return null;
        } catch (Exception $exc) {
            return null;
        }
    }
}
