<?php

namespace NERDDEV\Entidades;

class Vaga{
    
    private $cargo;
    private $descricao;
    private $qtdVagas;
    private $localTrabalho;
    private $dataFechamento;
    private $salario;
    private $status;

    public function __construct($cargo, $descricao, $qtdVagas, $localTrabalho, $dataFechamento, $salario, $status)
    {
      $this->cargo=  $cargo;
      $this->descricao= $descricao;
      $this->qtdVagas= $qtdVagas;
      $this->localTrabalho= $localTrabalho;
      $this->dataFechamento= $dataFechamento;
      $this->salario= $salario;
      $this->status= $status;
    }

    public function getCargo()
    {
        return $this->cargo;
    }
    public function getDescricao()
    {
        return $this->descricao;
    }
    public function getQtdvagas()
    {
        return $this->qtdVagas;
    }
    public function getLocalTrabalho()
    {
        return $this->localTrabalho;
    }
    public function getDataFechamento()
    {
        return $this->dataFechamento;
    }
    public function getSalario()
    {
        return $this->salario;
    }
    public function getStatus()
    {
        return $this->status;
    }
}