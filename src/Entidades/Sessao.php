<?php

namespace NERDDEV\Entidades;

class Sessao
{
    function __construct()
    { }

    function start()
    {
        return session_start();
    }

    public function add($chave, $valor)
    {
        $_SESSION[$chave] = $valor;
    }

    public function get($chave)
    {
        if (isset($_SESSION[$chave]))
            return $_SESSION[$chave];
        return '';
    }

    public function remove($chave)
    {
        if (isset($_SESSION[$chave]))
            session_unset($_SESSION[$chave]);
    }

    function del()
    {
       if(isset($_SESSION)){
        session_destroy();
       }
       
    }

    function existe($chave)
    {
        if (isset($_SESSION[$chave]))
            return true;
        return false;
    }
}
