<?php

namespace NERDDEV\Entidades;

class Usuario
{
    private $nome;
    private $email;
    private $senha;
    private $genero;
    private  $nacionalidade;
    private $dataNascimento;
    private $endereco;
    private $cidadeEstado;
    private $telefone;
    private $niveAcesso;


    public function __construct(
        $nome,
        $email,
        $senha,
        $genero,
        $nacionalidade,
        $dataNascimento,
        $endereco,
        $cidadeEstado,
        $telefone
    ) {
        $this->nome = $nome;
        $this->email = $email;
        $this->senha = $senha;
        $this->genero = $genero;
        $this->nacionalidade = $nacionalidade;
        $this->dataNascimento = $dataNascimento;
        $this->endereco = $endereco;
        $this->cidadeEstado = $cidadeEstado;
        $this->telefone = $telefone;
        $this->niveAcesso = 0;
    }

    public function getID()
    {
        return $this->id;
    }
    public function getNome()
    {
        return $this->nome;
    }

    public function getSenha()
    {
        return $this->senha;
    }
    public function getImagem()
    {
        return $this->imagem;
    }

    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    public function getdataNascimento()
    {
        return $this->dataNascimento;
    }

    public function getendereco()
    {
        return $this->endereco;
    }

    public function getCidadeEstado()
    {
        return $this->cidadeEstado;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getNivelAcesso()
    {
        return $this->niveAcesso;
    }
    public function getGenero()
    {
        return $this->genero;
    }
    public function getCurriculo()
    { }
}
