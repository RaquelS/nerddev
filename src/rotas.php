<?php

namespace NERDDEV;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


$rotas = new RouteCollection();

$rotas->add('index', new Route('/', [
    'controlador' => '\NERDDEV\Controladores\ControladorIndex',
    'metodo' => 'home'
]));
$rotas->add('home', new Route('/home', [
    'controlador' => '\NERDDEV\Controladores\ControladorIndex',
    'metodo' => 'home'
]));

$rotas->add('telaCadastrarUsuario', new Route('/formulario_cadastrar_usuario', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'telaCadastrarUsuario'
]));

$rotas->add('cadastrarUsuario', new Route('/cadastrar_usuario', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'cadastrarUsuario'
]));

$rotas->add('login', new Route('/login', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'login'
]));

$rotas->add('loginUsuario', new Route('/efetuar_login', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'efetuarLogin'
]));

$rotas->add('efetuarlogout', new Route('/efetuar_logout', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'logout'
]));

$rotas->add('perfil', new Route('/perfil', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'telaPerfil'
]));

$rotas->add('cadastrarCurriculo', new Route('/cadastrar_curriculo', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'cadastrarCurriculo'
]));

$rotas->add('painelAdmin', new Route('/painel_controle', [
    'controlador' => '\NERDDEV\Controladores\ControladorUsuario',
    'metodo' => 'PainelAdmin'
]));

$rotas->add('cadastrarVaga', new Route('/efetuar_cadastro_vaga', [
    'controlador' => '\NERDDEV\Controladores\ControladorVaga',
    'metodo' => 'cadastrarVaga'
]));

$rotas->add('telaCadastrarVaga', new Route('/cadastrar_vaga', [
    'controlador' => '\NERDDEV\Controladores\ControladorVaga',
    'metodo' => 'telaCadastrarVaga'
]));

$rotas->add('vaga', new Route('/vaga', [
    'controlador' => '\NERDDEV\Controladores\ControladorVaga',
    'metodo' => 'telaVaga'
]));