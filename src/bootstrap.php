<?php

namespace NERDDEV;

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use NERDDEV\Controladores\ControladorIndex;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use NERDDEV\Entidades\Sessao;

include 'rotas.php';

$session = new Sessao();
$session->start();

$loader = new FilesystemLoader('../src/Visoes');
$twig = new Environment($loader);

$response = new Response();

$contexto = new RequestContext();
$contexto->fromRequest(Request::createFromGlobals());

$mather = new UrlMatcher($rotas, $contexto);

try {
    $configRota = $mather->match($contexto->getPathInfo());
    $controlador = $configRota['controlador'];
    $controlador = new $controlador($response, $twig, Request::createFromGlobals());
    $metodo = $configRota['metodo'];
    if (isset($configRota['parametro']))
        $controlador->$metodo($configRota['parametro']);
    else
        $controlador->$metodo();
} catch (ResourceNotFoundException $ex) {
    $controlador = new ControladorIndex($response, $twig, Request::createFromGlobals());
    $controlador->paginaNaoEncontrada();
   
}

$response->send();
